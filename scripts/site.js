"use strict";

/**
 * description here
 * @author Uyen Dinh Michelle Banh
 * 
 */
document.addEventListener("DOMContentLoaded", setup);


function setup () { 
    const form = document.getElementsByTagName("form")[0];

    function createTable(rows, cols, color1, color2){
        
        const section_table = document.getElementById("table-here");
        const table = document.createElement("table");
        const table_body = document.createElement("tbody");
        section_table.textContent = undefined;
        section_table.appendChild(table);
        table.appendChild(table_body);

         
        for(let numRow = 0; numRow < rows; numRow++){
            const addRow = document.createElement("tr");
            table_body.appendChild(addRow);
            
            for(let numCol = 0; numCol < cols; numCol++){
                const addCol = document.createElement("td");
                addRow.appendChild(addCol);
                addCol.textContent = `${numRow},${numCol}`;
                if((numCol+numRow)%2 === 0){
                    addCol.style.backgroundColor = color1;
                }else{
                    addCol.style.backgroundColor = color2;
                }
            }
        }
    
    }

    form.addEventListener("submit", function(e){
        e.preventDefault(); 
        console.log(e);
        const rows = e.target.rows.value;
        const cols = e.target.cols.value;
        const color1 = e.target.color1.value;
        const color2 = e.target.color2.value;
        createTable(rows, cols, color1, color2);
    });

}
